<?php
require_once("dbconfig.php");

function getUserProfile($ID, $passWord) {
	global $db;
	$sql = "SELECT name, role, bad  FROM user WHERE ID=? and password=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ss", $ID, $passWord); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
			
	if ($row=mysqli_fetch_assoc($result)) {
		//return user profile
		$ret=array('uID' => $ID, 'uName' => $row['name'], 'uRole' => $row['role'], 'uBad' => $row['bad']);
	} else {
		//ID, password are not correct
		$ret=NULL;
	}
	return $ret;

}

function checkIDPwd($ID, $passWord)
{
	global $db;
	$isValid = false;
	$sql = "SELECT count(*) C FROM user WHERE uID=? and password=?";
	$stmt = mysqli_prepare($db, $sql);
	mysqli_stmt_bind_param($stmt, "ss", $ID, $passWord); // bind parameters with variables
	mysqli_stmt_execute("$stmt"); // run SQL
	$result = mysqli_stmt_get_result($stmt); //get the results

	if ($row = mysqli_fetch_assoc($result)) {

		if($row['C'] == 1) {
			$isValid = true;
		}
	}
	return $isValid;
}

function getUserStatus($ID) {
	$status = "You Guess!";
	return $status;
}

function addUser($ID, $passWord, $name, $role, $bad = 0){
		global $db;

		$sql = "SELECT * FROM user WHERE ID=? ";
		$stmt = mysqli_prepare($db, $sql); // prepare sql statement
		mysqli_stmt_bind_param($stmt, "s", $ID); //bind paramenters with variables
		mysqli_stmt_execute($stmt); // run SQL
		$result = mysqli_stmt_get_result($stmt); // get the results
		while ($row = mysqli_fetch_assoc($result)) {
			if ($row['ID'] == $ID)
			    return -1; // this ID is already used
		}
		$sql = "INSERT INTO `user` (`ID`, `password`, `name`, `role`, `bad`) VALUES (?, ?, ?, ?, 0)";
		$stmt = mysqli_prepare($db, $sql); // prepare sql statment
		mysqli_stmt_bind_param($stmt, "sssii", $ID, $passWord, $name, $role, $bad); // bind parameters with variables
		$result = mysqli_stmt_execute($stmt); // run SQL
		if ($result == false)
			return 1; // there's something wrong when adding user
		return 0;
}

function addBad($ID) {
	global $db;
	$sql="UPDATE user SET bad=bad+1 where ID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $ID); //bind parameters with variables
    return	mysqli_stmt_execute($stmt);  //執行SQL
}

?>