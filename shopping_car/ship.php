<?php
session_start();
require("prdModel.php");
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="text/html; charset=utf-8">
    <title>Shipping</title>
</head>
<body>
    [<a href="logout.php">Log Out</a>]

    <hr/ >
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
?>
    <table width="200" border="1">
        <tr>
            <td>ordID</td>
            <td>uID</td>
            <td>address</td>
            <td>ship</td>
        </tr>

<?php
$result=getShippedOrderList();
while ($rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>".$rs['ordID']."</td>";
	echo "<td>".$rs['uID']."</td>";
    echo "<td>".$rs['address']."</td>";
	echo "<td><a href='order.alreadyShipped.php?ordID=".$rs['ordID']."'>已寄送</a></td></tr>";
}
?>
<hr/>
</table>

</body>
</html>