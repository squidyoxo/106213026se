<?php
session_start();
require("cartModel.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="text/html; charset=utf-8">
    <title>Basic HTML</title>
</head>
<body>
    <h1>This is the cartControl page</h1>
    <hr/>
</body>
<?php
$act = $GET['act'];
$uID = $_SESSION["loginProfile"]['uID'];

if ($act == "add") {
    $prdID = $_GET['prdID'];

    if (addCartItem($uID, $prdID) == false) {
        echo "error exception: fail to add item \n";
    } else
        echo "add item successfully! \n";
        header ("Location: mainUI.php?act=addToCart");
} else if ($act == "checkout") {
    $address = $_POST['ADDRESS'];
    $status = 0;

    if (checkout($uID, $address) == false) {
        echo "error exception: fail to checkout \n";
    } else {
        echo "eheckout successfully! \n";
        header("Location: mainUI.php?act=checkout");
    }
} else {
    echo "error exception: error parameter 'act'";
}
?>
<br/>
<a href = "cartView.php" target="_self">Go to cart</a>
<a href = "mainUI.php" target="_self">Back to Main</a>
</html>