-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- 主機： localhost:8889
-- 產生時間： 2020 年 01 月 08 日 04:20
-- 伺服器版本： 5.7.26
-- PHP 版本： 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- 資料庫： `1081se`
--

-- --------------------------------------------------------

--
-- 資料表結構 `orderitem`
--

CREATE TABLE `orderitem` (
  `serno` int(11) NOT NULL,
  `ordID` int(11) NOT NULL,
  `prdID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `orderitem`
--

INSERT INTO `orderitem` (`serno`, `ordID`, `prdID`, `quantity`) VALUES
(3, 4, 1, 1),
(4, 4, 2, 1),
(8, 5, 0, 1),
(21, 0, 1, 1),
(22, 7, 2, 1),
(23, 9, 2, 1),
(24, 15, 3, 1),
(25, 7, 4, 1),
(26, 18, 4, 2),
(27, 19, 1, 2),
(28, 20, 1, 2),
(29, 21, 2, 1),
(30, 22, 2, 2),
(31, 23, 2, 2),
(32, 23, 1, 1),
(33, 24, 2, 1),
(34, 25, 1, 1),
(35, 26, 2, 1),
(36, 27, 2, 1),
(37, 28, 2, 1),
(38, 29, 1, 1),
(39, 30, 2, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `product`
--

CREATE TABLE `product` (
  `prdID` int(11) NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `detail` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `product`
--

INSERT INTO `product` (`prdID`, `name`, `price`, `detail`) VALUES
(1, 'iPhone 15', 800, 'This is a fake phone. Don\'t bu'),
(2, 'Water', 90, 'Pure water from Puli'),
(3, 'Air', 0, 'PM 2.5 air for free'),
(4, 'nothing', 20000, 'ohohoho');

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `ID` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `bad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `user`
--

INSERT INTO `user` (`ID`, `password`, `name`, `role`, `bad`) VALUES
('1233', '123', '123', 0, 0),
('admin', '123', '管理員', 9, 0),
('mama', '123', 'mama', 0, 0),
('qqqq', '123333', 'qqq', 0, 0),
('squid', 'qqq', 'sss', 1, 0),
('sss', '123', 'shipman', 2, 0),
('user', '123', '客戶', 1, 0),
('yarin', '333', 'yarin', 0, 0);

-- --------------------------------------------------------

--
-- 資料表結構 `userorder`
--

CREATE TABLE `userorder` (
  `ordID` int(11) NOT NULL,
  `uID` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `orderDate` date NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `userorder`
--

INSERT INTO `userorder` (`ordID`, `uID`, `orderDate`, `address`, `status`) VALUES
(5, 'admin', '0000-00-00', '', 0),
(23, 'user', '2020-01-08', 'oof', 3),
(24, 'user', '2020-01-08', 'm', 2),
(25, 'user', '2020-01-08', 'm', 2),
(26, 'user', '2020-01-08', ';;;;', 2),
(27, 'user', '2020-01-08', 'm', 2),
(29, 'user', '2020-01-08', 'sjsjsj', 1),
(30, 'user', '2020-01-08', '/z//z/zz/', 1);

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `orderitem`
--
ALTER TABLE `orderitem`
  ADD PRIMARY KEY (`serno`);

--
-- 資料表索引 `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prdID`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- 資料表索引 `userorder`
--
ALTER TABLE `userorder`
  ADD PRIMARY KEY (`ordID`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `orderitem`
--
ALTER TABLE `orderitem`
  MODIFY `serno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `product`
--
ALTER TABLE `product`
  MODIFY `prdID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `userorder`
--
ALTER TABLE `userorder`
  MODIFY `ordID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;