<?php
require_once("dbconfig.php");

function getOrderList($uID) {
	global $db;
	// echo $uID;
	$sql = "SELECT ordID, orderDate, status FROM userorder WHERE uID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getConfirmedOrderList() {
	global $db;
	$sql = "SELECT ordID, uID, orderDate FROM userorder WHERE status=1";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	//mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getShippedOrderList() {
	global $db;
	$sql = "SELECT ordID, uID, address FROM userorder WHERE status=2 ORDER BY address";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	//mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function _getCartID($uID) {
	//get an unfished order (status=0) from userOrder
	global $db;
	//echo $uID;
	$sql = "SELECT ordID FROM `userorder` WHERE uID=? and status=0";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
	$result = mysqli_stmt_get_result($stmt); //get the results
	// echo $result;
	if ($row=mysqli_fetch_assoc($result)) {
		return $row["ordID"];
	} else {
		//no order with status=0 is found, which means we need to creat an empty order as the new shopping cart
		$sql = "INSERT INTO `userorder` (uID, orderDate, address, status) values (?, '2019-01-01', '',  '0');";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
		mysqli_stmt_execute($stmt);  //執行SQL
		$newOrderID=mysqli_insert_id($db);
		return $newOrderID;
	}
}

function CountQuantity($uID, $prdID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "SELECT quantity FROM orderitem WHERE ordID = ? AND prdID = ?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID);
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	$count =mysqli_fetch_assoc($result);
	return $count['quantity'];
}

function CountTotal($prdID, $quantity) {
	global $db;
	$sql = "SELECT orderitem.prdID, product.name,  orderitem.quantity FROM orderitem, product WHERE ordID = ? AND prdID = ? AND orderitem.prdID = product.prdID;";
	$stmt = mysqli_prepare($db, $sql);
	mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID);
	mysqli_stmt_execute($stmt);
}

function addToCart($uID, $prdID) {
	global $db;
	$ordID= _getCartID($uID);
	// echo $prdID;
	$sql = "INSERT INTO orderItem (ordID, prdID, quantity) values (?,?,1);";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function removeFromCart($serno) {
	global $db;
	$sql = "DELETE from orderItem where orderitem.serno=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $serno); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function removeFromOrder($ordID) {
    global $db;
    $sql="delete from userorder where ordID=?";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
    return	mysqli_stmt_execute($stmt);  //執行SQL
}



function checkout($uID, $address) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "UPDATE userorder set orderDate=now(),address=?,status=1 where ordID=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "si", $address, $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function shipout($ordID) {
	global $db;
	$sql = "UPDATE userorder set status=2 where ordID=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function alreadyship($ordID) {
	global $db;
	$sql = "UPDATE userorder SET status=3 where ordID=?";
	$stmt = mysqli_prepare($db, $sql);
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function getCartDetail($uID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql="SELECT orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}


function getOrderDetail($ordID) {
	global $db;
	$sql="SELECT orderitem.serno, product.name, product.price, orderItem.quantity from orderitem, product where orderitem.prdID=product.prdID and orderitem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function UpdateQuantity($uID, $prdID, $count) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "UPDATE orderitem set quantity=? where ordID=? AND prdID =?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "iii", $count, $ordID, $prdID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}

function getOrderAddress($ordID) {
	global $db;
	$sql="SELECT address FROM userorder WHERE ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	$addr =mysqli_fetch_assoc($result);
	return $addr['address'];
}
?>